// ---------------------------------------------------------------------------------------------------------
#ifndef QT_SIGHT_WIDGET_H_MRV
#define QT_SIGHT_WIDGET_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <uFunctions.h>
// ---------------------------------------------------------------------------------------------------------
#include <QPainter>
#include <QObject>
#include <QFrame>
#include <QRect>
// ---------------------------------------------------------------------------------------------------------
#include <vector>
// ---------------------------------------------------------------------------------------------------------
#include <assert.h>
// ---------------------------------------------------------------------------------------------------------

class QSightWidget : public QFrame
{
public:
   enum { UNKNOWN_ID = -1 };
   // ------------------------------------------------------------------------------------------------------
   enum zPositions
   {
      Z_NOWHERE   = 0,  ///< �����
      Z_ABOVE     = 1,  ///< ��� ��������
      Z_UNDER     = 2   ///< ��� ��������
   };
   // ------------------------------------------------------------------------------------------------------

   class QTarget : public QObject
   {
   public:
      QTarget ( QObject * parent_p = NULL ) : QObject( parent_p ) { ; }
      virtual void draw ( QPainter * painter_p, const QRect & borders_p ) { ; }
   };
   // ------------------------------------------------------------------------------------------------------

   typedef std::map< std::size_t, QTarget * > targetsContainers_map;
   // ------------------------------------------------------------------------------------------------------
private:
   targetsContainers_map targetsAbove_m, targetsUnder_m;
   // ------------------------------------------------------------------------------------------------------

   void paintEvent ( QPaintEvent * event_p )
   {
      QPainter painter_l( this );
      QRect workArea_l( - width() / 2, - height() / 2, width(), height() );
      painter_l.setRenderHint( QPainter::Antialiasing );
      painter_l.translate( workArea_l.bottomRight() );
      for ( targetsContainers_map::iterator it = targetsUnder_m.begin(); targetsUnder_m.end() != it; ++it ) 
         it->second->draw( &painter_l, workArea_l );
      drawSight( &painter_l, workArea_l );
      for ( targetsContainers_map::iterator it = targetsAbove_m.begin(); targetsAbove_m.end() != it; ++it ) 
         it->second->draw( &painter_l, workArea_l );
      QWidget::paintEvent( event_p );
   }
   // ------------------------------------------------------------------------------------------------------

   QTarget * target ( targetsContainers_map & container_p, std::size_t id_p )
   {
      targetsContainers_map::const_iterator found_l = container_p.find( id_p );
      return container_p.end() != found_l ? found_l->second : NULL; 
   }
   // ------------------------------------------------------------------------------------------------------

   bool delTarget ( targetsContainers_map & container_p, std::size_t id_p ) 
   {
      targetsContainers_map::const_iterator found_l = container_p.find( id_p );
      if ( container_p.end() == found_l ) return false;
      container_p.erase( found_l );
      update();
      return true;
   }
   // ------------------------------------------------------------------------------------------------------
protected:
   
   QTarget * aboveTarget ( std::size_t id_p ) { return target( targetsAbove_m, id_p ); }
   // ------------------------------------------------------------------------------------------------------

   bool delAboveTarget ( std::size_t id_p ) { return delTarget( targetsAbove_m, id_p ); }
   //-------------------------------------------------------------------------------------------------------

   QTarget * underTarget ( std::size_t id_p ) { return target( targetsUnder_m, id_p ); }
   // ------------------------------------------------------------------------------------------------------

   bool delUnderTarget ( std::size_t id_p ) { return delTarget( targetsUnder_m, id_p ); }
   // ------------------------------------------------------------------------------------------------------

   virtual void drawSight ( QPainter * painter_p, const QRect & borders_p ) { ; }
   // ------------------------------------------------------------------------------------------------------
public:

   QSightWidget ( QWidget * parent_p = NULL ) : QFrame( parent_p ) { ; }
   // ------------------------------------------------------------------------------------------------------

   std::size_t addTarget ( QTarget * target_p, zPositions zPositions_p )
   {
      std::size_t id_l = UNKNOWN_ID;
      assert( NULL != target_p );
      switch ( zPositions_p )
      {
         case Z_ABOVE: id_l = utl::smartMapInsert( target_p, targetsAbove_m ); break;
         case Z_UNDER: id_l = utl::smartMapInsert( target_p, targetsUnder_m ); break;
         default: ;
      }
      if ( UNKNOWN_ID == id_l ) return id_l;
      if ( target_p->parent() != this ) target_p->setParent( this );
      update();
      return id_l;
   }
   // ------------------------------------------------------------------------------------------------------
}; // class QSightWidget
// ---------------------------------------------------------------------------------------------------------
#endif // QT_SIGHT_WIDGET_H_MRV
// ---------------------------------------------------------------------------------------------------------