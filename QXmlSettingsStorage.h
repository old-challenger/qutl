// ---------------------------------------------------------------------------------------------------------
#ifndef QT_XML_SETTINGS_STORAGE_H_MRV
#define QT_XML_SETTINGS_STORAGE_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!	
\file		QXmlSettingsStorage.h
\author	MRV
\date		07.11.2010
\version	0.1
\brief	������ ������-��������� QXmlSettingsStorage< class data_struct, class attaching_functor > ��� 
         �������� � ���������� �������� ����������.
*/
// ---------------------------------------------------------------------------------------------------------
#include <uShared.h>
#include <QConvertors.h>
//
#include <QLatin1String>
#include <QDomDocument>
#include <QTextStream>
#include <QDomElement>
#include <QDomText>
#include <QDomNode>
#include <QDomAttr>
#include <QFile>
//
#include <map>
#include <stack>
#include <string>
#include <assert.h>
// ---------------------------------------------------------------------------------------------------------

class abstractDataReference
{
public:
   virtual ~ abstractDataReference () {};
   virtual const QString get () const = 0;
   virtual void set ( const QString & value_�p ) = 0;
   virtual void reset () = 0;
};
// ---------------------------------------------------------------------------------------------------------

template < class value_type, template < class > class convert_functor >
class dataReference : public abstractDataReference
{
private:
   value_type  defaultValue_m,
               & value_rm;
public:
   dataReference ( value_type & value_rp, value_type defaultValue_p ) : 
      value_rm( value_rp ), defaultValue_m( defaultValue_p ) 
   {  ; }
   const QString get () const { return convert_functor< QString >()( value_rm ); }
   void set ( const QString & value_cp ) 
   {  value_rm = convert_functor< value_type >()( value_cp ); }
   void reset () { value_rm = defaultValue_m; }
};
// ---------------------------------------------------------------------------------------------------------

class xmlAttribute
{
friend class xmlElement;
private:
   QString                 name_m;
   abstractDataReference * valueReference_m;
   // ------------------------------------------------------------------------------------------------------

   xmlAttribute ( const QString & name_cp, abstractDataReference * valueReference_p ) :
      name_m            ( name_cp            ),
      valueReference_m  ( valueReference_p   )
   {  assert( NULL != valueReference_m ); }
   // ------------------------------------------------------------------------------------------------------
public:
   xmlAttribute ( const QString & name_cp = QLatin1String( "no_name" ) ) : 
      name_m( name_cp ), valueReference_m( NULL ) 
   { ; }
   // ------------------------------------------------------------------------------------------------------
   ~ xmlAttribute () { if ( NULL != valueReference_m ) delete valueReference_m; }
   // ------------------------------------------------------------------------------------------------------
   const QString & name () const { return name_m; }
   const QString value () const 
   {  return NULL == valueReference_m ? QString() : valueReference_m->get(); }
   // ------------------------------------------------------------------------------------------------------
   void setName ( const QString & name_cp ) { name_m = name_cp; }
   void setValue ( const QString & value_cp ) 
   {  if ( NULL != valueReference_m ) valueReference_m->set( value_cp ); }
   // ------------------------------------------------------------------------------------------------------
}; // class xmlAttribute
// ---------------------------------------------------------------------------------------------------------

class xmlElement : public xmlAttribute
{
public:
   typedef utl::shared< xmlElement >         element_sh;
   typedef utl::shared< xmlAttribute >       attribute_sh;
   typedef std::map< QString, element_sh >   elements_map;
   typedef std::map< QString, attribute_sh > attributes_map;
   // ------------------------------------------------------------------------------------------------------
private:
   typedef utl::shared< elements_map >    elements_map_sh;
   typedef utl::shared< attributes_map >  attributes_map_sh;
   // ------------------------------------------------------------------------------------------------------
   attributes_map_sh attributes_m;
   elements_map_sh   elements_m;
   // ------------------------------------------------------------------------------------------------------

   xmlElement ( const QString & name_cp, abstractDataReference * valueReference_p ) : 
      xmlAttribute( name_cp, valueReference_p ), attributes_m(), elements_m()  
   { ; }
   // ------------------------------------------------------------------------------------------------------
public:
   xmlElement () : xmlAttribute(), attributes_m(), elements_m() { ; }
   // ------------------------------------------------------------------------------------------------------

   xmlElement ( const QString & name_cp ) : xmlAttribute( name_cp ), attributes_m(), elements_m() { ; }
   // ------------------------------------------------------------------------------------------------------
   const attributes_map & constAttributes () const { return attributes_m.constData(); }
   const elements_map & constElements () const { return elements_m.constData(); }
   // ------------------------------------------------------------------------------------------------------
   attributes_map & attributes () { return attributes_m.data(); }
   elements_map & elements () { return elements_m.data(); }
   // ------------------------------------------------------------------------------------------------------

   template < class value_type, template< class > class convert_functor >
   void addAttribute ( const QString & name_cp, value_type defaultValue_p, value_type & reference_rp ) 
   {  
      attributes_m.data().insert( std::make_pair( name_cp, attribute_sh( new xmlAttribute( name_cp, 
         new dataReference< value_type, convert_functor >( reference_rp, defaultValue_p ) ) ) ) ); 
   }
   // ------------------------------------------------------------------------------------------------------
   
   template < class value_type >
   void addAttribute ( const QString & name_cp, value_type defaultValue_p, value_type & reference_rp ) 
   {  addAttribute< value_type, QConvertors::string >( name_cp, defaultValue_p, reference_rp ); }
   // ------------------------------------------------------------------------------------------------------

   template < class value_type >
   void addAttribute ( const QString & name_cp, value_type & reference_rp ) 
   {  addAttribute< value_type, QConvertors::string >( name_cp, reference_rp, reference_rp ); }
   // ------------------------------------------------------------------------------------------------------

   element_sh addElement ( const QString & name_cp ) 
   {  
      element_sh element_l( new xmlElement( name_cp ) );
      elements_m.data().insert( std::make_pair( name_cp, element_l ) ); 
      return element_l;
   }
   // ------------------------------------------------------------------------------------------------------

   template < class value_type, template< class > class convert_functor >
   element_sh addElement ( const QString & name_cp, value_type defaultValue_p, value_type & reference_rp ) 
   {  
      element_sh element_l( new xmlElement( name_cp, new dataReference< value_type, convert_functor >( 
         reference_rp, defaultValue_p ) ) );
      elements_m.data().insert( std::make_pair( name_cp, element_l ) ); 
      return element_l;
   }
   // ------------------------------------------------------------------------------------------------------

   template < class value_type >
   element_sh addElement ( const QString & name_cp, value_type defaultValue_p, value_type & reference_rp ) 
   {  return addElement< value_type, QConvertors::string >( name_cp, defaultValue_p, reference_rp ); }
   // ------------------------------------------------------------------------------------------------------

   template < class value_type >
   element_sh addElement ( const QString & name_cp, value_type & reference_rp ) 
   {  return addElement< value_type, QConvertors::string >( name_cp, reference_rp, reference_rp ); }
   // ------------------------------------------------------------------------------------------------------
}; // class xmlElement
// ---------------------------------------------------------------------------------------------------------

template < class data_struct, class attaching_functor >
class QXmlSettingsStorage
{
private:
   xmlElement xmlDocStructure_m;
   // ------------------------------------------------------------------------------------------------------
public:
   QXmlSettingsStorage ( data_struct & settings_rp ) : xmlDocStructure_m()
   {  attaching_functor()( settings_rp, xmlDocStructure_m ); }
   // ------------------------------------------------------------------------------------------------------

   void save ( const std::wstring & fileName_cp ) 
   {
      QDomDocument   xmlDoc_l( xmlDocStructure_m.name() );
      QDomElement    childElement_l; 
      QDomNode       parentNode_l = xmlDoc_l;
      //
      xmlElement::elements_map::iterator  elementIt   = xmlDocStructure_m.elements().begin(),
                                          endElemntIt = xmlDocStructure_m.elements().end();
      //
      std::stack< QDomNode >                             nodesStack_l;
      std::stack< xmlElement::elements_map::iterator >   currentElementsStack_l,
                                                         endElementsStack_l;
      while ( currentElementsStack_l.size() || endElemntIt != elementIt )
      {
         if ( endElemntIt != elementIt )
         {
            // ������� ��������
            childElement_l = xmlDoc_l.createElement( elementIt->first );
            parentNode_l.appendChild( childElement_l );
            // ��������� ����
            const QString text_cl = elementIt->second.constData().value();
            if ( text_cl.length() ) childElement_l.appendChild( xmlDoc_l.createTextNode( text_cl ) );
            // ��������
            xmlElement::attributes_map::const_iterator attributeIt = 
               elementIt->second.constData().constAttributes().begin();
            for ( ; elementIt->second.constData().constAttributes().end() != attributeIt; ++attributeIt )
               childElement_l.setAttribute( attributeIt->first, attributeIt->second.constData().value() );
            //
            xmlElement::element_sh elementBuffer_l = ( elementIt++ )->second;
            // ��������� ������� ��������
            if ( elementBuffer_l.constData().constElements().size() )
            {
               nodesStack_l.push( parentNode_l );
               currentElementsStack_l.push( elementIt );
               endElementsStack_l.push( endElemntIt );
               //
               parentNode_l = childElement_l;
               elementIt = elementBuffer_l.data().elements().begin();
               endElemntIt = elementBuffer_l.data().elements().end();
            }
         }
         else
         {  // �������������� �����������(��������) ������ ��������
            assert( nodesStack_l.size() && currentElementsStack_l.size() && endElementsStack_l.size() );
            parentNode_l = nodesStack_l.top();
            elementIt = currentElementsStack_l.top();
            endElemntIt = endElementsStack_l.top();
            //
            nodesStack_l.pop();
            currentElementsStack_l.pop();
            endElementsStack_l.pop();
         }
      }
      //
      QFile file_l( QString::fromUtf16( ( const unsigned short * ) fileName_cp.c_str() ) );
      if ( !file_l.open( QIODevice::WriteOnly ) ) { return ; }
      QTextStream ( &file_l ) << xmlDoc_l.toString();
      file_l.close();
   }
   // ------------------------------------------------------------------------------------------------------

   bool load ( const std::wstring & fileName_cp ) 
   {
      // �������� �����
      QFile file_l( QString::fromUtf16( ( const unsigned short * ) fileName_cp.c_str() ) );
      if ( !file_l.open( QIODevice::ReadOnly ) ) return false;
      // ������ xml-���������
      QDomDocument xmlDoc_l;
      std::size_t unhandledNodesCounter_l = 0;
      if ( xmlDoc_l.setContent( &file_l ) ) 
      {
         // ��������� �������� ����������
         QDomNode currentXmlNode_l = xmlDoc_l.documentElement();
         xmlElement::element_sh parentElement_l = xmlDocStructure_m;
         //
         std::stack< QDomNode >                 nodesStack_l;
         std::stack< xmlElement::element_sh >   parentElementsStack_l;
         //
         bool goNextNode_l = true;
         unhandledNodesCounter_l = parentElement_l.constData().constElements().size();
         while ( nodesStack_l.size() || !currentXmlNode_l.isNull() )
         {
            goNextNode_l = true;
            if ( currentXmlNode_l.isNull() )
            {
               currentXmlNode_l = nodesStack_l.top();
               parentElement_l = parentElementsStack_l.top();
               parentElementsStack_l.pop();
               nodesStack_l.pop();
            }
            else 
            {
               if ( currentXmlNode_l.isText() )
               {
                  const QDomText currentXmlTextElement_l = currentXmlNode_l.toText();
                  parentElement_l.data().setValue( currentXmlTextElement_l.data() );
               }
               else if ( currentXmlNode_l.isElement() )  
               {
                  const QDomElement currentXmlElement_l = currentXmlNode_l.toElement();
                  //const QString tagName_cl = currentXmlElement_l.tagName();
                  xmlElement::elements_map::iterator elementIt_l = 
                     parentElement_l.data().elements().find( currentXmlElement_l.tagName() );
                  //
                  if ( parentElement_l.constData().constElements().end() != elementIt_l )
                  {
                     assert( unhandledNodesCounter_l );
                     --unhandledNodesCounter_l;
                     // �������� �������� ��������� ��� �������� xml-��������
                     unhandledNodesCounter_l += elementIt_l->second.constData().constAttributes().size();
                     xmlElement::attributes_map::iterator attributeIt_l = 
                        elementIt_l->second.data().attributes().begin();
                     QString attributeValue_l;
                     for ( ; elementIt_l->second.constData().constAttributes().end() != attributeIt_l; 
                           ++attributeIt_l )
                     {
                        attributeValue_l = currentXmlElement_l.attribute( attributeIt_l->first );
                        if ( !attributeValue_l.isNull() ) 
                        {
                           attributeIt_l->second.data().setValue( attributeValue_l );
                           assert( unhandledNodesCounter_l );
                           --unhandledNodesCounter_l;
                        }
                     }
                     // �������� �� ������� ���������� ������ ��������
                     const QDomNode childXmlNode_cl = currentXmlElement_l.firstChild();
                     if ( !childXmlNode_cl.isNull() )
                     {
                        goNextNode_l = false;
                        unhandledNodesCounter_l += elementIt_l->second.constData().constElements().size();
                        //
                        nodesStack_l.push( currentXmlNode_l );
                        parentElementsStack_l.push( parentElement_l );
                        //
                        currentXmlNode_l = childXmlNode_cl;
                        parentElement_l = elementIt_l->second;
                     }
                  } // if ( parentElement_l.constData().constElements().end() != elementIt_l )
               } // if ( currentXmlNode_l.isText() ) .. else if ( currentXmlNode_l.isElement() )  
            } // if ( currentXmlNode_l.isNull() ) .. else 
            if ( goNextNode_l ) currentXmlNode_l = currentXmlNode_l.nextSibling();
         } // while ( nodesStack_l.size() || !currentXmlNode_l.isNull() )
      } // if ( xmlDoc_l.setContent( &file_l ) ) 
      file_l.close();
      return !unhandledNodesCounter_l;
   }
   // ------------------------------------------------------------------------------------------------------
}; // class QXmlSettingsStorage
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_SETTINGS_H_MRV
// ---------------------------------------------------------------------------------------------------------
