// ---------------------------------------------------------------------------------------------------------
#ifndef LABELED_WIDGET_H_MRV 
#define LABELED_WIDGET_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <QLabel>
#include <QWidget>
#include <QBoxLayout>
// ---------------------------------------------------------------------------------------------------------

template< class widget_t, QBoxLayout::Direction direction_p = QBoxLayout::TopToBottom >
class QLabeledWidget : public QWidget
{
private:
	QLabel		label_m;
	widget_t	   widget_m;
	QBoxLayout	layout_m;
	// ------------------------------------------------------------------------------------------------------

	void init ( /*bool stretch_p*/ )
	{
      layout_m.setMargin( 0 );
		layout_m.addWidget( &label_m,    0 );
      layout_m.addWidget( &widget_m,   1/*stretch_p ? 1 : 0*/ );
	}
	// ------------------------------------------------------------------------------------------------------
public:
	QLabeledWidget ( QWidget * parent_p = NULL ) :
		QWidget	( parent_p ),
		label_m	(),
		widget_m (),s
		layout_m ( direction_p, this )
	{	init( /*true*/ ); }
	// ------------------------------------------------------------------------------------------------------

	QLabeledWidget ( const QString & labelTxt_p, bool addStretch_p = false, QWidget * parent_p = NULL ) :
		QWidget	( parent_p     ),
		label_m	( labelTxt_p   ),
		widget_m (),
		layout_m ( direction_p, this )
	{	
      init( /*!addStretch_p*/ ); 
      if ( addStretch_p ) layout_m.addStretch( 1 );
   }
	// ------------------------------------------------------------------------------------------------------

	inline const QLabel & constLabel () const { return label_m; }
	// ------------------------------------------------------------------------------------------------------

	inline QLabel & label () { return label_m; } 
	// ------------------------------------------------------------------------------------------------------

	inline const widget_t & constWidget () const { return widget_m; }
	// ------------------------------------------------------------------------------------------------------

	inline widget_t & widget () { return widget_m; } 
	// ------------------------------------------------------------------------------------------------------

	inline const QLayout & constLayout () const { return layout_m; }
	// ------------------------------------------------------------------------------------------------------

	inline QLayout & layout () { return layout_m; } 
	// ------------------------------------------------------------------------------------------------------
}; // class QLabeledWidget
// ---------------------------------------------------------------------------------------------------------
#endif // LABELED_WIDGET_H_MRV
// ---------------------------------------------------------------------------------------------------------