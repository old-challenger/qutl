// ---------------------------------------------------------------------------------------------------------
#ifndef QT_PIPE_SLIDER_H_MRV
#define QT_PIPE_SLIDER_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <uShared.h>
// ---------------------------------------------------------------------------------------------------------
#include <QAbstractSlider>
#include <QLinearGradient>
#include <QFontMetrics>
#include <QMouseEvent>
#include <QPainter>
#include <QString>
#include <QColor>
// ---------------------------------------------------------------------------------------------------------

class QPipeSlider : public QAbstractSlider
{
public:
   class QScale
   {
      friend class QPipeSlider;
      friend class utl::shared< QScale >;
      // ---------------------------------------------------------------------------------------------------
   private:
      int heigh_m, minWidth_m;
      // ---------------------------------------------------------------------------------------------------

      int heigh ( const QPainter & painter_p ) 
      {	
         const QFontMetrics metrics_l( painter_p.font() );
         return heigh_m = metrics_l.height();
      }
      // ---------------------------------------------------------------------------------------------------		

      int minWidth () const { return minWidth_m; }
      // ---------------------------------------------------------------------------------------------------

      void draw ( QPainter & painter_p ) 
      {
         const QString	minStr_l = makeStrByValue ( slider_m->minimum() ),
                        maxStr_l = makeStrByValue ( slider_m->maximum() );
         QFontMetrics metrics_l( painter_p.font() );
         const int   minWidth_l = metrics_l.width( minStr_l ), // ����� ������� ������������ ��������
                     maxWidth_l = metrics_l.width( maxStr_l ); // ����� ������� ������������� ��������
         // ���������� � ������������� ����������� ����� ��������
         minWidth_m = minWidth_l + maxWidth_l;
         if ( slider_m->rect().width() < minWidth_m ) slider_m->setMinimumWidth( minWidth_m );
         // ��������� ������ ���������
         const int   textPositionY_l = slider_m->rect().y() + 0.8 * heigh_m,
                     maxPositionX_l = slider_m->rect().topRight().x() - maxWidth_l;
         painter_p.drawText( slider_m->rect().x(), textPositionY_l, minStr_l );
         painter_p.drawText( maxPositionX_l, textPositionY_l, maxStr_l );
         // ��������� �������� ��������
         const QString valueStr_cl = makeStrByValue ( slider_m->value() );
         const int valueWigth_cl = metrics_l.width( valueStr_cl );
         if ( slider_m->rect().width() < minWidth_m + valueWigth_cl ) return ;
         const int   interval_cl = slider_m->maximum() - slider_m->minimum(),
                     valuePos_cl = 0 == interval_cl ? 0 
                        : static_cast< float >( slider_m->value() ) 
                              / interval_cl * slider_m->rect().width() - valueWigth_cl / 2;
         if ( valuePos_cl < minWidth_l || valuePos_cl + valueWigth_cl > maxPositionX_l ) return ;
         painter_p.drawText( valuePos_cl, textPositionY_l, valueStr_cl );
      }
      // ---------------------------------------------------------------------------------------------------
   protected:
      QPipeSlider * slider_m;

      virtual const QString makeStrByValue ( int value_p ) { return QString::number( value_p ); }
      // ---------------------------------------------------------------------------------------------------

      virtual ~QScale () { }
      // ---------------------------------------------------------------------------------------------------
   public: 

      QScale () : slider_m( NULL ), heigh_m( 0 ), minWidth_m( 0 ) {}
      // ---------------------------------------------------------------------------------------------------
   };	// class QScale
   // ------------------------------------------------------------------------------------------------------
private:
   bool     scaleIsEnable_m;  ///< ����� ��������/���������
   //int      progressHeight_m; ///< ������ ������ 
   QColor   progressColor_m,	///< ���
            shadowColor_m;    ///< ���������� 

   utl::shared< QScale > scale_m; ///< ������ ��������� �����
   // ------------------------------------------------------------------------------------------------------

protected:
   int interval () const { return maximum() - minimum(); }
   // ------------------------------------------------------------------------------------------------------

   QLinearGradient createGrad ( const QColor & color_p, const QRect & rect_cp )
   {
      QLinearGradient result_l( rect_cp.topLeft(), rect_cp.bottomLeft() );
      result_l.setColorAt( 0,		color_p );
      result_l.setColorAt( 0.25,	color_p.light( 213 ) );
      result_l.setColorAt( 1,		color_p );
      return result_l;
   }
   // ------------------------------------------------------------------------------------------------------

   virtual void drawBackground ( QPainter & painter_p, const QRect & rect_cp )
   {
      painter_p.setPen( Qt::transparent );
      // ������� ������� (���������)
      const QLinearGradient progressGrad_cl = createGrad( progressColor_m, rect_cp );
      // ��������� ������� ������
      painter_p.setBrush( progressGrad_cl );
      painter_p.drawRect( rect_cp );
   }
   // ------------------------------------------------------------------------------------------------------

   void paintEvent ( QPaintEvent * event_p )
   {
      // ������ ��� ���������
      QPainter painter_l( this );
      // ��������� �������� �������
      const int scaleHeight_l = scaleIsEnable_m ? scale_m.data().heigh( painter_l ) : 0;
      // ������� ����������� ���������� ���������
      QRect drawRect_l( rect() );
      // ��������� �����
      if ( scaleHeight_l && scaleHeight_l < height() )
      {
         painter_l.setPen( Qt::black );
         scale_m.data().draw( painter_l );
         drawRect_l.setTop( drawRect_l.top() + scaleHeight_l );
      }
      // ���
      drawBackground ( painter_l, drawRect_l );
      // ���������� 
      QColor shadow_l( Qt::darkBlue );
      shadow_l.setAlpha( 157 );
      painter_l.setBrush( shadow_l );
      drawRect_l.setX( 0 == interval() ? 0 
                        : static_cast< float >( value() ) / interval() * drawRect_l.width() );
      painter_l.drawRect( drawRect_l );
   }
   // ------------------------------------------------------------------------------------------------------

   void mouseReleaseEvent ( QMouseEvent * event_p )
   {
      if ( Qt::LeftButton == event_p->button() && rect().contains( event_p->pos() ) )
      {
         int newValue_l = static_cast< float >( event_p->pos().x() ) / rect().width() * interval();
         setValue( newValue_l );
         sliderChange( QAbstractSlider::SliderValueChange );
         emit sliderReleased();
      }
   }
   // ------------------------------------------------------------------------------------------------------
public:

   QPipeSlider ( QWidget * parent_p = NULL ) :
      QAbstractSlider   ( parent_p ),
      scaleIsEnable_m   ( true ),
      progressColor_m	( Qt::darkGreen   ),	
      shadowColor_m		( Qt::darkBlue	   ),	
      scale_m	         ()
   {  shadowColor_m.setAlpha( 157 ); }
   // ------------------------------------------------------------------------------------------------------

   bool scaleIsEnable () const { return scaleIsEnable_m; }
   // ------------------------------------------------------------------------------------------------------

   QScale & scale () { return scale_m.data(); }
   // ------------------------------------------------------------------------------------------------------

   void setScaleEnable ( bool value_p ) { scaleIsEnable_m = value_p; }
   // ------------------------------------------------------------------------------------------------------

   void setProgressColor ( const QColor & color_p ) { progressColor_m = color_p; }
   // ------------------------------------------------------------------------------------------------------

   void setShadowColor ( const QColor & color_p ) { shadowColor_m = color_p; }
   // ------------------------------------------------------------------------------------------------------

   void setScale( QScale * scale_p )
   {
      scale_m.makeNew( scale_p );
      scale_m.data().slider_m = this;
   }
   // ------------------------------------------------------------------------------------------------------
}; // class QPipeSlider
// ---------------------------------------------------------------------------------------------------------
#endif // QT_PIPE_SLIDER_H_MRV
// ---------------------------------------------------------------------------------------------------------