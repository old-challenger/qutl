// ---------------------------------------------------------------------------------------------------------
#ifndef MARKED_SLIDER_H_MRV
#define MARKED_SLIDER_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <QPipeSlider.h>
// ---------------------------------------------------------------------------------------------------------
//#include <QStyle>
//#include <QWidget>
//#include <QStyleOptionFrame>
// ---------------------------------------------------------------------------------------------------------
#include <vector>
#include <algorithm>
// ---------------------------------------------------------------------------------------------------------

class QMarkedSlider : public QPipeSlider
{
public:

	class marker_pair
	{
	private:
		int beginVal_m, endVal_m;
		// --------------------------------------------------------------------------------------------------
	public:

		marker_pair () : beginVal_m( 0 ), endVal_m( 0 ) {}
		// --------------------------------------------------------------------------------------------------

		marker_pair ( int begin_p, int end_p ) : beginVal_m( begin_p ), endVal_m( end_p ) {}
		// --------------------------------------------------------------------------------------------------

		marker_pair ( const marker_pair & etalon_p ) :
			beginVal_m	( etalon_p.beginVal_m	), 
			endVal_m	   ( etalon_p.endVal_m		) 
		{ 	}
		// --------------------------------------------------------------------------------------------------

		int begin () const { return beginVal_m; }
		// --------------------------------------------------------------------------------------------------

		int end () const { return endVal_m; }
		// --------------------------------------------------------------------------------------------------

		void setEnd ( int value_p ) { endVal_m = value_p; }
		// --------------------------------------------------------------------------------------------------

		void draw ( QPainter & painter_p, const QRect & rect_p, float interval_p ) const
		{
			const QRect markedRect_l(	
						QPoint( 0 == interval_p ? 0 : static_cast< float >( beginVal_m ) 
									/ interval_p * rect_p.width(), rect_p.top() ),
						QPoint( 0 == interval_p ? 0 : static_cast< float >( endVal_m ) 
									/ interval_p * rect_p.width(), rect_p.bottom() ) );
			painter_p.drawRect( markedRect_l );
		}
		// --------------------------------------------------------------------------------------------------
	};	// class cMarker ..
	// ------------------------------------------------------------------------------------------------------
	typedef std::vector< marker_pair >		markers_vector;
	typedef utl::shared< markers_vector >	markers_sh;
	// ------------------------------------------------------------------------------------------------------
private:
	QColor		markersColor_m;	///< �������
	markers_sh	markersArray_m;	///< ������ ��������
	// ------------------------------------------------------------------------------------------------------
protected:

	void drawBackground ( QPainter & painter_p, const QRect & rect_cp )
	{
		QPipeSlider::drawBackground ( painter_p, rect_cp );
		const QLinearGradient markersGrad_cl = createGrad( markersColor_m, rect_cp );
		// �������
		painter_p.setBrush( markersGrad_cl );
		for (	markers_vector::const_iterator it = markersArray_m.constData().begin();
				it != markersArray_m.constData().end(); ) 
			(it++)->draw( painter_p, rect_cp, interval() );
	}
	// ------------------------------------------------------------------------------------------------------
public:

	QMarkedSlider ( QWidget * parent_p = NULL ) :
		QPipeSlider		( parent_p 		),
		markersColor_m	( Qt::darkRed	),	
		markersArray_m	()
	{	}
	// ------------------------------------------------------------------------------------------------------
	
	void setMarkersColor ( const QColor & color_p ) { markersColor_m = color_p; }
	// ------------------------------------------------------------------------------------------------------

	void setMarkers ( const markers_sh & newArray_p ) { markersArray_m = newArray_p; }
	// ------------------------------------------------------------------------------------------------------

	void addMarker ( const marker_pair & newMarker_p ) { markersArray_m.data().push_back( newMarker_p ); }
	// ------------------------------------------------------------------------------------------------------

	void clearMarkers () { markersArray_m.data().clear(); }
	// ------------------------------------------------------------------------------------------------------
};	// class QMarkedSlider
// ---------------------------------------------------------------------------------------------------------
#endif // MARKED_SLIDER_H_MRV
// ---------------------------------------------------------------------------------------------------------
