// ---------------------------------------------------------------------------------------------------------
#ifndef QT_FUNCTORS_H_MRV
#define QT_FUNCTORS_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <QConvertors.h>
#include <QDateTime>
#include <QDir>
// ---------------------------------------------------------------------------------------------------------
namespace QFunctors {
// ---------------------------------------------------------------------------------------------------------

class QCheckDirExists
{
public:
   template < class in_string_class >
   bool operator () ( const in_string_class & dirName_cp ) const 
   {
      const QDir dirOperator_cl;
      const QString dirName_cl = QConvertors::string< QString >()( dirName_cp );
      return dirOperator_cl.exists( dirName_cl ) ? true : dirOperator_cl.mkdir( dirName_cl );
   }
   // ------------------------------------------------------------------------------------------------------
}; // class QCheckDirExists
// ---------------------------------------------------------------------------------------------------------

template < class out_string_class >
class QMakeTempFileName
{
public:
   template < class in_string_class >
   out_string_class operator () (   const in_string_class & rootDirName_cp,
                                    const in_string_class & subdirNameFormat_cp, 
                                    const in_string_class & fileNameFormat_cp,
                                    const in_string_class & fileExtention_cp, int counterSize_p ) const
   {
      const QDateTime currentDate_cl = QDateTime::currentDateTime();
      const QString  rootDirName_cl = QConvertors::string< QString >()( rootDirName_cp ),
                     fullDirName_cl = rootDirName_cl 
                        + ( rootDirName_cl.length() ? QLatin1String( "/" ) : QString() )
                        + QConvertors::string< QString >()( currentDate_cl.toString( subdirNameFormat_cp ) ),
                     fileTimePart_cl = fullDirName_cl
                        + ( fullDirName_cl.length() ? QLatin1String( "/" ) : QString() ) 
                        + QConvertors::string< QString >()( currentDate_cl.toString( fileNameFormat_cp ) );
      QCheckDirExists()( fullDirName_cl );
      unsigned int fileNumber_l = 0;
      QString fileName_l;
      while ( QFile::exists( fileName_l = fileTimePart_cl + QLatin1String( "[" )
         + QString::number( ++fileNumber_l ).rightJustified( counterSize_p, '0' ) + QLatin1String( "]" )
         + fileExtention_cp ) )
      {  ; }
      return QConvertors::string< out_string_class >()( fileName_l );
   }
};
// ---------------------------------------------------------------------------------------------------------
} // namespace QFunctors 
// ---------------------------------------------------------------------------------------------------------
#endif // QT_FUNCTORS_H_MRV
// ---------------------------------------------------------------------------------------------------------