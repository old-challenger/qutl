//----------------------------------------------------------------------------------------------------------
#ifndef QT_WIDGETS_OPERATIONS_H_MRV
#define QT_WIDGETS_OPERATIONS_H_MRV
//----------------------------------------------------------------------------------------------------------
#include <QString>
#include <QSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QDateTimeEdit>
//----------------------------------------------------------------------------------------------------------

template < class out_type, class widget_type >
class defaultConvertStrategy 
{
public:
   static void     set ( const out_type & out_cp, widget_type & widget_cp );
   static out_type get ( const widget_type & widget_cp  );
};
//----------------------------------------------------------------------------------------------------------

template <>
class defaultConvertStrategy< int, QComboBox >
{
public:
   static void set ( const int & out_cp, QComboBox & widget_p ) { widget_p.setCurrentIndex( out_cp ); }
   static int get ( const QComboBox & widget_cp  ) { return widget_cp.currentIndex(); }
}; 
//----------------------------------------------------------------------------------------------------------

template <>
class defaultConvertStrategy< int, QSpinBox >
{
public:
   static void set ( const int & out_cp, QSpinBox & widget_p ) { widget_p.setValue( out_cp ); }
   static int get ( const QSpinBox & widget_cp ) { return widget_cp.value(); }
}; 
//----------------------------------------------------------------------------------------------------------

template <>
class defaultConvertStrategy< QString, QCheckBox >
{
public:
   static const QString & trueTxt () 
   {  static const QString txt_scl = QLatin1String( "true" ); return txt_scl; }
   // ------------------------------------------------------------------------------------------------------   

   static const QString & falseTxt () 
   {  static const QString txt_scl = QLatin1String( "false" ); return txt_scl; }
   // ------------------------------------------------------------------------------------------------------   

   static void set ( const QString & out_cp, QCheckBox & widget_p ) 
   {  widget_p.setChecked( trueTxt() == out_cp ); }
   // ------------------------------------------------------------------------------------------------------

   static QString get ( const QCheckBox & widget_cp  )
   {  return widget_cp.isChecked() ? trueTxt() : falseTxt(); }
   // ------------------------------------------------------------------------------------------------------
}; // class defaultConvertStrategy< QCheckBox, QString >
//----------------------------------------------------------------------------------------------------------

template <>
class defaultConvertStrategy< QString, QComboBox >
{
public:

   static void set ( const QString & out_cp, QComboBox & widget_p )
   {  widget_p.setCurrentIndex( out_cp.toInt() ); }
   // ------------------------------------------------------------------------------------------------------

   static QString get ( const QComboBox & widget_cp  ) 
   {  return QString::number( widget_cp.currentIndex() ); }
   // ------------------------------------------------------------------------------------------------------
}; // class defaultConvertStrategy< QCheckBox, QString >
//----------------------------------------------------------------------------------------------------------

template <>
class defaultConvertStrategy< QString, QDateTimeEdit >
{
public:

   static void set ( const QString & out_cp, QDateTimeEdit & widget_p )
   {  widget_p.setDateTime( QDateTime::fromTime_t( out_cp.toUInt() ) ); }
   // ------------------------------------------------------------------------------------------------------

   static QString get ( const QDateTimeEdit & widget_cp  ) 
   {  return QString::number( widget_cp.dateTime().toTime_t() ); }
   // ------------------------------------------------------------------------------------------------------
}; // class defaultConvertStrategy< QCheckBox, QString >
//----------------------------------------------------------------------------------------------------------

template <>
class defaultConvertStrategy< QString, QLineEdit >
{
public:
   static void set ( const QString & out_cp, QLineEdit & widget_p ) { widget_p.setText( out_cp ); }
   static QString get ( const QLineEdit & widget_cp  ) { return widget_cp.text(); }
}; 
//----------------------------------------------------------------------------------------------------------

template <>
class defaultConvertStrategy< QString, QSpinBox >
{
public:
   static void set ( const QString & out_cp, QSpinBox & widget_p ) { widget_p.setValue( out_cp.toInt() ); }
   static QString get ( const QSpinBox & widget_cp ) { return widget_cp.text(); }
}; 
//----------------------------------------------------------------------------------------------------------

template <>
class defaultConvertStrategy< QString, QTimeEdit >
{
public:

   static void set ( const QString & out_cp, QTimeEdit & widget_p )
   {  widget_p.setDateTime( QDateTime::fromTime_t( out_cp.toUInt() ) ); }
   // ------------------------------------------------------------------------------------------------------

   static QString get ( const QTimeEdit & widget_cp  ) 
   {  return QString::number( widget_cp.dateTime().toTime_t() ); }
   // ------------------------------------------------------------------------------------------------------
}; // class defaultConvertStrategy< QTimeEdit, QString >
//----------------------------------------------------------------------------------------------------------

template <  class value_type, class widget_type, 
            template < class, class > class convert_strategy = defaultConvertStrategy >
class QWidgetsOperations
{
private:
   QWidgetsOperations () {}
   //-------------------------------------------------------------------------------------------------------
public:
   static void setValue( const value_type & value_cp, widget_type & widget_p )
   {  convert_strategy< value_type, widget_type >::set( value_cp, widget_p ); }
   //-------------------------------------------------------------------------------------------------------

   static value_type getValue ( const widget_type & widget_cp )
   {  return convert_strategy< value_type, widget_type >::get( widget_cp ); }
   // ------------------------------------------------------------------------------------------------------
}; // class QWidgetsOperations
//----------------------------------------------------------------------------------------------------------
#endif // SET_QT_STRING_VALUE_H_MRV
//----------------------------------------------------------------------------------------------------------