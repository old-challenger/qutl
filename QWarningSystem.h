// ---------------------------------------------------------------------------------------------------------
#ifndef QT_WARNING_SYSTEM_H_MRV
#define QT_WARNING_SYSTEM_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <uFunctions.h>
#include <QFunctors.h>
// ---------------------------------------------------------------------------------------------------------
#include <QApplication>
#include <QMutexLocker>
#include <QMessageBox>
#include <QDateTime>
#include <QThread>
#include <QObject>
#include <QTimer>
#include <QMutex>
#include <QFile>
#include <QDir>
// ---------------------------------------------------------------------------------------------------------
#include <queue>
#include <map>
// ---------------------------------------------------------------------------------------------------------
#include <assert.h>
// ---------------------------------------------------------------------------------------------------------

class QWarningSystem 
{
public:
   enum { BAG_ID = -1 };
	// ------------------------------------------------------------------------------------------------------

   enum flags
   {
      FLAGS_NOT_SET           = 0x0,
      FLAG_DELETE_AFTER_USING = 0x1,
      FLAG_SHOW_MODAL_DIALOG  = 0x2,
      FLAG_WRITE_TO_LOG_FILE  = 0x4,
      OUT_FLAGS               = FLAG_SHOW_MODAL_DIALOG | FLAG_WRITE_TO_LOG_FILE
   };
   // ------------------------------------------------------------------------------------------------------
private:
   class messagesGroup
   {
   private:
      QMessageBox::Icon importance_m;
      QString           typeTxt_m;
      int               flags_m;
      // ---------------------------------------------------------------------------------------------------
   public:

      messagesGroup () : 
         importance_m   (),
         typeTxt_m      (),
         flags_m        ( FLAGS_NOT_SET )
      {  }
      // ---------------------------------------------------------------------------------------------------

      messagesGroup ( const messagesGroup & etalon_cp ) :
         importance_m   ( etalon_cp.importance_m ),
         typeTxt_m      ( etalon_cp.typeTxt_m    ),
         flags_m        ( etalon_cp.flags_m      )
      {  }
      // ---------------------------------------------------------------------------------------------------
      messagesGroup( QMessageBox::Icon importance_p, QString typeTxt_p, int flags_p ) :
         importance_m   ( importance_p ),
         typeTxt_m      ( typeTxt_p    ),
         flags_m        ( flags_p      )
      {  }
      // ---------------------------------------------------------------------------------------------------

      int flags () const { return flags_m; }
      // ---------------------------------------------------------------------------------------------------

      QMessageBox::Icon importance () const { return importance_m; } 
      // ---------------------------------------------------------------------------------------------------

      QString text () const { return typeTxt_m; }
      // ---------------------------------------------------------------------------------------------------
   }; // class messagesGroup
   // ------------------------------------------------------------------------------------------------------

   class messagesSettings
   {
   public:
      enum { WARNING_SYSTEM_ERRORS_GROUP = 0 };
   private:
      QString     outputDir_m,
                  commonNamePart_m;          
      // ---------------------------------------------------------------------------------------------------
   public:
      std::map< std::size_t, messagesGroup >  massagesGroups;
      // ---------------------------------------------------------------------------------------------------

      messagesSettings () : 
         outputDir_m       (),
         commonNamePart_m  (),
         massagesGroups    ()
      {  
         massagesGroups.insert( std::make_pair( WARNING_SYSTEM_ERRORS_GROUP, 
            messagesGroup( QMessageBox::Critical, QObject::tr( "���������� ������ ������� ����������" ), 
                           FLAG_SHOW_MODAL_DIALOG ) ) ); 
      } 
      // ---------------------------------------------------------------------------------------------------

      const QString & outputDir () const { return outputDir_m; }
      // ---------------------------------------------------------------------------------------------------

      const QString & commonNamePart () const { return commonNamePart_m; }
      // ---------------------------------------------------------------------------------------------------

      void setOutputDir ( const QString & value_cp ) { outputDir_m = value_cp; }
      // ---------------------------------------------------------------------------------------------------

      void setCommonNamePart ( const QString & value_cp ) { commonNamePart_m = value_cp; }
      // ---------------------------------------------------------------------------------------------------
   }; // class messagesSettings
   // ------------------------------------------------------------------------------------------------------

   class message
   {
   private:
      std::size_t groupID_m;
      QString	   text_m;	  
      int         flags_m;
      // ---------------------------------------------------------------------------------------------------
   public:
      
      message ( const message & etalon_cp ) :
         groupID_m( etalon_cp.groupID_m   ),
         text_m   ( etalon_cp.text_m      ),
         flags_m  ( etalon_cp.flags_m     )
      {  }
      // ---------------------------------------------------------------------------------------------------

      message ( std::size_t groupID_p, const QString text_cp, int flags_p ) :
         groupID_m( groupID_p ),
         text_m   ( text_cp   ),
         flags_m  ( flags_p   )
      {  }
      // ---------------------------------------------------------------------------------------------------

      int flags () const { return flags_m; }
      // ---------------------------------------------------------------------------------------------------
         
      const std::size_t group () const { return groupID_m; }
      // ---------------------------------------------------------------------------------------------------

      const QString & text () const { return text_m; }
      // ---------------------------------------------------------------------------------------------------
   }; // class message
   // ------------------------------------------------------------------------------------------------------

   static QMutex & blocker () { static QMutex blocker_m; return blocker_m; }
   // ------------------------------------------------------------------------------------------------------

   static messagesSettings & settings () { static messagesSettings setting_sl; return setting_sl; }
   // ------------------------------------------------------------------------------------------------------

   static std::queue< message > & messages () { static std::queue< message > queue_sl; return queue_sl; }
   // ------------------------------------------------------------------------------------------------------

   static QString messageType2Str ( QMessageBox::Icon importance_p )
   {
      switch ( importance_p )
      {
         case QMessageBox::Critical:      return "[ERROR]\t- ";
         case QMessageBox::Information:   return "[EVENT]\t- ";
         case QMessageBox::Warning:       return "[WARNING]\t- ";
         default:                         return "[UNKNOWN]\t- ";
      }
   }
   // ------------------------------------------------------------------------------------------------------
   
   QWarningSystem () { ; }
   // ------------------------------------------------------------------------------------------------------
public:

   static void addMessageToQueue (  std::size_t goup_p, const QString & text_cp, 
                                    int flags_p = FLAGS_NOT_SET )
   {
      blocker().lock();
      messages().push( message( goup_p, text_cp, flags_p ) );
      blocker().unlock();
   }
   // ------------------------------------------------------------------------------------------------------

   static void addMessageToQueue (  QString title_p, QString text_p, 
                                    QMessageBox::Icon importance_p = QMessageBox::Warning,
                                    int flags_p = FLAG_SHOW_MODAL_DIALOG )
   {
      const std::size_t goupID_cl = createMessagesGroup( importance_p, title_p, 
                                                         FLAG_DELETE_AFTER_USING | flags_p );
      addMessageToQueue( goupID_cl, text_p, FLAGS_NOT_SET );
   }
   // ------------------------------------------------------------------------------------------------------

   static std::size_t createMessagesGroup (  QMessageBox::Icon importance_p, 
                                             const QString & typeTxt_cp, int flags_p ) 
   { 
      blocker().lock();
      const std::size_t id_cl = utl::smartMapInsert( 
         messagesGroup( importance_p, typeTxt_cp, flags_p ), settings().massagesGroups ); 
      blocker().unlock();
      return id_cl;
   }
   // ------------------------------------------------------------------------------------------------------

   static void makeMessage (  QString title_p, QString text_p, 
                              QMessageBox::Icon importance_p = QMessageBox::Warning,
                              int flags_p = FLAG_SHOW_MODAL_DIALOG )
   {
      if ( FLAG_WRITE_TO_LOG_FILE & flags_p )
      {
         const QDateTime currentDate_cl = QDateTime::currentDateTime();
         const QString fileName_cl = settings().outputDir() 
                           + ( settings().outputDir().length() ? QLatin1String( "/" ) : QString() ) 
                           + currentDate_cl.toString( "yyyy-MM-dd.log" );
         QFile outFile_l( fileName_cl );
         if ( outFile_l.open( QFile::WriteOnly | QIODevice::Append ) )
         {
            outFile_l.write( (   currentDate_cl.toString( "hh:mm:ss - " ) + messageType2Str( importance_p ) 
                              +  title_p + " - " + text_p + "\n" ).toLocal8Bit() );
            outFile_l.close();
         }
      }
      if ( FLAG_SHOW_MODAL_DIALOG & flags_p )
      {
         QMessageBox messageDialog_l( importance_p, title_p, text_p );
         messageDialog_l.exec();
      }
   }
   // ------------------------------------------------------------------------------------------------------

   static int makeMessage ( const message & message_cp )
   {
      std::map< std::size_t, messagesGroup >::const_iterator found_l = 
         settings().massagesGroups.find( message_cp.group() );
      assert( settings().massagesGroups.end() != found_l );
      const int realFlags_cl = FLAGS_NOT_SET == message_cp.flags() 
                                 ? found_l->second.flags() : message_cp.flags();
      makeMessage( found_l->second.text(), message_cp.text(), found_l->second.importance(), realFlags_cl );
      if ( FLAG_DELETE_AFTER_USING & found_l->second.flags() ) 
      {
         blocker().lock();
         utl::mapDelete( message_cp.group(), settings().massagesGroups );
         blocker().unlock();
      }
      return realFlags_cl;
   }
   // ------------------------------------------------------------------------------------------------------

   static int makeMessage ( std::size_t group_p, const QString & text_cp, int flags_p = FLAGS_NOT_SET )
   {  return makeMessage( message( group_p, text_cp, flags_p ) ); }
   // ------------------------------------------------------------------------------------------------------

   static unsigned int processAllMessagesInQueue ()
   {
      if ( QThread::currentThread() != QApplication::instance()->thread() ) return 0;
      blocker().lock();
      std::size_t count_l = messages().size();
      blocker().unlock();
      int realFlags_l = FLAGS_NOT_SET;
      unsigned int logMessages_l = 0;
      for ( std::size_t i = count_l; i != 0 ; --i )
      {
         blocker().lock();
         message message_l( messages().front() );
         messages().pop();
         blocker().unlock();
         realFlags_l = makeMessage( message_l );
         if ( FLAG_WRITE_TO_LOG_FILE & realFlags_l ) ++logMessages_l;
      }
      return logMessages_l;
   }
   // ------------------------------------------------------------------------------------------------------

   static void setOutputDir ( const QString & dirName_cp )
   {
      if ( dirName_cp.isEmpty() ) return ;
      if ( QFunctors::QCheckDirExists()( dirName_cp ) ) 
         settings().setOutputDir( dirName_cp );
      else
      {
         static const QString errorTxt_scl = QObject::tr( "�� ������� �������� ������� dirName_cp \"" );
         addMessageToQueue( messagesSettings::WARNING_SYSTEM_ERRORS_GROUP, errorTxt_scl + dirName_cp 
            + QLatin1String( "\"." ) );
      }
   }
   // ------------------------------------------------------------------------------------------------------
}; // class QWarningSystem 
// ---------------------------------------------------------------------------------------------------------
#endif // QT_WARNING_SYSTEM_H_MRV
// ---------------------------------------------------------------------------------------------------------