// ---------------------------------------------------------------------------------------------------------
#ifndef QT_TYPES_CONVERTORS_H_MRV
#define QT_TYPES_CONVERTORS_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <QString>
#include <QObject>
#include <assert.h>
// ---------------------------------------------------------------------------------------------------------
namespace QConvertors {
// ---------------------------------------------------------------------------------------------------------

class consts
{
private: consts() { ; }
public: 
   static const QString & trueTxt () 
   {  static const QString value_scl = QLatin1String( "true" ); return value_scl; }
   static const QString & falseTxt ()
   {  static const QString value_scl = QLatin1String( "false" ); return value_scl; }
};
// ---------------------------------------------------------------------------------------------------------

template < class out_t >
class simple 
{
public:
   template < class in_t >
   out_t operator () ( const in_t & in_cp ) const { return static_cast< out_t >( in_cp ); }
};
// ---------------------------------------------------------------------------------------------------------

template < class out_t > 
class string { public: template < class in_t > out_t operator () ( const in_t & in_cp ) const; };
// ---------------------------------------------------------------------------------------------------------

template <> 
class string< int > 
{  
public: 
   template < class in_t > int operator () ( const in_t & in_cp ) const;
   template <> int operator ()< QString > ( const QString & in_cp ) const { return in_cp.toInt(); } 
};
// ---------------------------------------------------------------------------------------------------------

template <> 
class string< bool > 
{  
public: 
   template < class in_t > bool operator () ( const in_t & in_cp ) const;
   template <>
   bool operator ()< QString > ( const QString & in_cp  ) const { return consts::trueTxt() == in_cp; } 
};
// ---------------------------------------------------------------------------------------------------------

template <> 
class string< char > 
{  
public: 
   template < class in_t > char operator () ( const in_t & in_cp ) const;
   template <>
   char operator ()< QString > ( const QString & in_cp  ) const 
   { 
      assert( 1 == in_cp.length() );
      return in_cp[ 0 ].toLatin1(); 
   } 
};
// ---------------------------------------------------------------------------------------------------------

template <> 
class string< double > 
{  
public: 
   template < class in_t > double operator () ( const in_t & in_cp ) const;
   template <>
   double operator ()< QString > ( const QString & in_cp  ) const { return in_cp.toDouble(); } 
};
// ---------------------------------------------------------------------------------------------------------

template <> 
class string< unsigned int > 
{  
public: 
   template < class in_t > unsigned int operator () ( const in_t & in_cp ) const;
   template <> 
   unsigned int operator ()< QString > ( const QString & in_cp  ) const { return in_cp.toUInt(); } 
};
// ---------------------------------------------------------------------------------------------------------

template <>
class string< std::string >
{
public:
   template < class in_t > std::string operator () ( const in_t & in_cp ) const;
   template <> 
   std::string operator ()< QString > ( const QString & in_cp  ) const { return in_cp.toLatin1().data(); } 
};
// ---------------------------------------------------------------------------------------------------------

template <>
class string< std::wstring >
{
public:
   template < class in_t > std::wstring operator () ( const in_t & in_cp ) const;
   template <> 
   std::wstring operator ()< QString > ( const QString & in_cp  ) const 
   {  return ( const wchar_t * ) in_cp.utf16(); } 
};
// ---------------------------------------------------------------------------------------------------------

template <> 
class string< QString > 
{  
public: 
   template < class in_t > 
   QString operator () ( const in_t & in_cp ) const { return QString::number( in_cp ); }

   template <> 
   QString operator ()< bool > ( const bool & in_cp ) const 
   {  return in_cp ? consts::trueTxt() : consts::falseTxt(); }

   template <> 
   QString operator ()< char > ( const char & in_cp ) const 
   {  return QString( QChar::fromLatin1( in_cp ) ); }

   template <> 
   QString operator ()< QString > ( const QString & in_cp  ) const { return in_cp; } 

   template <>
   QString operator ()< std::string > ( const std::string & in_cp ) const { return in_cp.c_str(); }

   template <>
   QString operator ()< std::wstring > ( const std::wstring & in_cp ) const 
   {  return QString::fromUtf16( ( const ushort * ) in_cp.c_str() ); }
};
// ---------------------------------------------------------------------------------------------------------
}; // namespace QConvertors {
// ---------------------------------------------------------------------------------------------------------
#endif // QT_TYPES_CONVERTORS_H_MRV
// ---------------------------------------------------------------------------------------------------------

